<?php

namespace Tests;

use CHEZ14\PixivPhp\Client;
use PHPUnit\Framework\TestCase;

/**
 * Class ClientTest.
 *
 * @covers \CHEZ14\PixivPhp\Client
 */
class ClientTest extends TestCase
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var array
     */
    protected $options;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->options = [];
        $this->client = new Client($this->options);
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown(): void
    {
        parent::tearDown();

        unset($this->client);
        unset($this->options);
    }

    public function testSend(): void
    {
        /** @todo This test is incomplete. */
        $this->markTestIncomplete();
    }

    public function testGet(): void
    {
        /** @todo This test is incomplete. */
        $this->markTestIncomplete();
    }
}
