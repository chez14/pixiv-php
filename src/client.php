<?php

namespace CHEZ14\PixivPhp;

use GuzzleHttp\Client as GuzzleHttpClient;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class Client
{

    public $baseUrl = "https://www.pixiv.net/";
    public $apiUrl = "/ajax/";

    /**
     * GuzzleClient
     *
     * @var GuzzleHttpClient
     */
    protected $httpclient = null;

    public function __construct(array $options)
    {
        $this->httpclient = new GuzzleHttpClient([
            "base_uri" => (rtrim($this->baseUrl, "/") . "/" . trim($this->apiUrl, "/") . "/")
        ]);
    }

    public function send(RequestInterface $request, array $options): ResponseInterface
    {
        return $this->httpclient->send($request, $options);
    }

    public function get(string $url, array $query): ResponseInterface
    {
        return $this->httpclient->request("get", $url, ["query" => $query]);
    }
}
