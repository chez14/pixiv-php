<?php

namespace CHEZ14\PixivPhp\Util;

use CHEZ14\PixivPhp\Client;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Utils;
use GuzzleHttp\RequestOptions;

class ImageFetcher
{

    /**
     * Image Downloader Helpers
     *
     * @param string $imageUrl the url of the image
     * @param string|null $destination destination file, if blank, the data stream will be returned instead
     * @param Client $client HTTP Client
     * @return string will return destination path, or image datastream.
     */
    public function fetch(string $imageUrl, ?string $destination, Client $client): string
    {
        $referer = $client->baseUrl;

        $reqOptions = [];

        if ($destination) {
            $sink = Utils::tryFopen($destination, "w");
            $reqOptions[RequestOptions::SINK] = $sink;
        }

        $req = new Request("GET", $imageUrl, [
            "Referer" => $referer,
        ]);

        $response = $client->send($req, $reqOptions);

        if ($destination) {
            return null;
        }

        return $response->getBody();
    }
}
